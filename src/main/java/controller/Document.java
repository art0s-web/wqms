package controller;

public class Document {
	private String body;
	private String title;
	private String description;
	
	public String getBody() { return this.body; }
	void setBody(String body) { this.body = body; }
	public String getTitle() { return this.title; }
	void setTitle(String title) { this.title = title; }
	public String getDescription() { return this.description; }
	void setDescription(String description) { this.description = description; }
}
