package controller;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.Optional;

public class WebsiteScanner {
	private Optional<Document> document;
	
	public WebsiteScanner(String url) {
		var response = this.scan(url);
		this.document = Optional.empty();
		
		if(!response.isEmpty()) {
			var responseContents = response.orElseThrow();
			var document = this.extractInformations(responseContents);
			this.document = Optional.of(document);
		}
	}
	
	private Optional<HttpResponse<String>> scan(String url){
		try {
			HttpClient client = HttpClient.newBuilder()
					.version(Version.HTTP_1_1)
					.followRedirects(Redirect.NORMAL)
					.connectTimeout(Duration.ofSeconds(10))
					.build();
			
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(url))
					.build();
			
			var response = client.send(request, BodyHandlers.ofString());	
			return Optional.of(response);	
		} catch(Exception e) {
			return Optional.empty();
		}
	}
	
	private Document extractInformations(HttpResponse<String> response) {
		var document = new Document();
		var body = response.body().toString();
		
		document.setBody(body);
		document.setTitle(this.searchForTag("title", body));
		document.setDescription(this.searchForAttribute("meta", "description", body));
		return document;
	}
	
	
	private String searchForTag(String tag, String html) {
		var startTag = "<" + tag + ">";
		var postionOfStartTag = html.indexOf(startTag); 
		var postionOfEndTag = html.indexOf("</" + tag + ">");
		
		var foundString = html.substring(postionOfStartTag, postionOfEndTag);
		var stringList = foundString.split(startTag);
		var contentsRightFromStartTag = stringList[stringList.length - 1];
		return contentsRightFromStartTag;
	}
	
	private String searchForAttribute(String tag, String attribute, String html) {
		var postitionOfAttribute = html.indexOf(attribute);
		var foundString = html.substring(postitionOfAttribute);
		var leftAndRightFromDeclaration = foundString.split("content=", 2);
		var contentsRight = leftAndRightFromDeclaration[leftAndRightFromDeclaration.length - 1];
		var leftAndRightAfterClosingTag = contentsRight.split(">");
		var contentsLeft = leftAndRightAfterClosingTag[0];
		
		var builder = new StringBuilder();
		var doubleQuotes = builder.append('"').toString();
		var valueOfAttribute = contentsLeft.replace(doubleQuotes,"");
		return valueOfAttribute;
	}
	

	public Optional<Document> getDocument() { return this.document; }
}
